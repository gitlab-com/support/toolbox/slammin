FROM ubuntu:focal
LABEL maintainer="jyoung@gitlab.com"
SHELL ["/bin/bash", "-c"]


### UPDATES ###
ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
RUN apt-get update -y && apt-get install -y --no-install-recommends apt-utils
### RUN UPGRADES
RUN apt-get upgrade -y

### BASE TOOLS ###
RUN yes | unminimize
RUN apt-get install -y --no-install-recommends \
  apt-utils \
  ca-certificates \
  openssl \
  curl \
  wget \
  git \
  man \
  manpages-posix \
  locales

### DEVELOPER TOOLS ###
RUN apt-get install -y --no-install-recommends \
  build-essential \
  zlib1g-dev \
  libssl-dev \
  libreadline-dev \
  libyaml-dev \
  libxml2-dev \
  libxslt-dev \
  libffi-dev \
  libarchive-tools

### DISTRO UTILS ###
# ubuntu:focal base already includes gzip, tar, sed
# add a set of distribution utilities as individual layers
RUN apt-get install -y --no-install-recommends vim
RUN apt-get install -y --no-install-recommends jq
RUN apt-get install -y --no-install-recommends jid
RUN apt-get install -y --no-install-recommends gawk
RUN apt-get install -y --no-install-recommends less
RUN apt-get install -y --no-install-recommends bash-completion
RUN apt-get install -y --no-install-recommends fzf
RUN apt-get install -y --no-install-recommends tmux
RUN apt-get install -y --no-install-recommends unzip
RUN apt-get install -y --no-install-recommends mc
RUN apt-get install -y --no-install-recommends strace
RUN apt-get install -y --no-install-recommends lsof
RUN apt-get install -y --no-install-recommends silversearcher-ag
RUN apt-get install -y --no-install-recommends daemontools
#  https://github.com/sharkdp/bat/issues/938
RUN apt-get install -y --no-install-recommends  -o Dpkg::Options::="--force-overwrite" bat ripgrep
RUN ln -s /usr/bin/batcat /usr/bin/bat
RUN apt-get install -y --no-install-recommends dnsutils
RUN apt-get install -y --no-install-recommends host
RUN apt-get install -y --no-install-recommends net-tools
RUN apt-get install -y --no-install-recommends nmap

### DISTRO RUBY
#RUN apt-get install -y --no-install-recommends ruby ruby-dev

### ASDF / ASDF TOOLS
ADD .tool-versions /root/.tool-versions
RUN git clone --depth 1 https://github.com/asdf-vm/asdf.git $HOME/.asdf && \
  echo '. $HOME/.asdf/asdf.sh' >> $HOME/.bashrc && \
  echo '. $HOME/.asdf/asdf.sh' >> $HOME/.profile && \
  source ~/.bashrc
ENV PATH="${PATH}:/root/.asdf/shims:/root/.asdf/bin"
WORKDIR /root
RUN asdf plugin add ruby && asdf install ruby

### DOWNLOAD UTILS ###
COPY checksums /root/dlutils/checksums
# strace-parser
RUN wget -c https://gitlab.com/gitlab-com/support/toolbox/strace-parser/uploads/aeec995aa6d28ce0ddc5e8dab3952278/strace-parser_0-7-2_linux_musl.tar.gz -O /root/dlutils/strace-parser.tgz
RUN sha256sum -c /root/dlutils/checksums/strace-parser.sha256.txt
RUN tar xzvf /root/dlutils/strace-parser.tgz -C /usr/local/bin
# fast-stats
RUN wget -c https://gitlab.com/gitlab-com/support/toolbox/fast-stats/uploads/bc80367b4592d1e4b6b275a222c850ee/fast-stats_0-7-3_linux_musl_x86_64.tar.gz -O /root/dlutils/fast-stats.tgz
RUN sha256sum -c /root/dlutils/checksums/fast-stats.sha256.txt
RUN tar xzvf /root/dlutils/fast-stats.tgz -C /root/dlutils
RUN mv /root/dlutils/fast-stats_0-7-3_linux_musl_x86_64/fast-stats /usr/local/bin/fast-stats
# angle-grinder
RUN wget -c https://github.com/rcoh/angle-grinder/releases/download/v0.18.0/agrind-x86_64-unknown-linux-gnu.tar.gz -O /root/dlutils/agrind.tgz
RUN sha256sum -c /root/dlutils/checksums/agrind.sha256.txt
RUN tar xzvf /root/dlutils/agrind.tgz -C /usr/local/bin
# lnav
RUN wget -c https://github.com/tstack/lnav/releases/download/v0.10.1/lnav-0.10.1-musl-64bit.zip -O /root/dlutils/lnav.zip
RUN sha256sum -c /root/dlutils/checksums/lnav.sha256.txt
RUN cd /root/dlutils && unzip /root/dlutils/lnav.zip && mv /root/dlutils/lnav-0.10.1/lnav /usr/local/bin
# git-sizer
RUN wget -c https://github.com/github/git-sizer/releases/download/v1.5.0/git-sizer-1.5.0-linux-amd64.zip -O /root/dlutils/git-sizer.zip
RUN sha256sum -c /root/dlutils/checksums/git-sizer.sha256.txt
RUN cd /root/dlutils && unzip /root/dlutils/git-sizer.zip -d git-sizer && mv /root/dlutils/git-sizer/git-sizer /usr/local/bin

# cleanup
RUN rm -rfv /root/dlutils

### RUBY TOOLS
ADD Gemfile /root/Gemfile
ADD Gemfile.lock /root/Gemfile.lock
WORKDIR /root
RUN gem install bundler && bundle install
RUN bundle binstubs --all --path /usr/local/bin

### SHELL CUSTOMIZATION ###
# bash customizations
ADD .bash_customize /root/.bash_customize
RUN echo "source /root/.bash_customize" >> /root/.bashrc
ENV TERM xterm-256color

### RUN UPGRADES
RUN apt-get update -y && apt-get upgrade -y